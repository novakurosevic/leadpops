## System requirements:

- PHP >= 7.1.3 
- BCMath PHP Extension 
- Ctype PHP Extension 
- JSON PHP Extension 
- Mbstring PHP Extension 
- OpenSSL PHP Extension 
- PDO PHP Extension 
- Tokenizer PHP Extension 
- XML PHP Extension
- CURL PHP Extension


Also you need to enable modul pretty urls in LAMP server (sudo a2enmod rewrite).
Restart apache if you enable this modul.

__________________________________________________________________________________________________________________



Project settings:
1. In project root set .env database parameters (DB_DATABASE, DB_USERNAME and DB_PASSWORD)

In project root run following commands:

2. Run
`composer install`

3. Run
`php artisan migrate`

4. Run
`php artisan db:seed --class=UsersTableSeeder`


__________________________________________________________________________________________________________________


Accessing project:

1. Run command:
`php artisan serve`

2. Go to route 1:
POST `http://localhost:8000/api/loginapi`

In POST request set field `email` and value `test@test.com` and field `password` with value `123`.
As response you'll get token value (JWT).

3. Set JWT in header request and go to Route 2.
POST `http://localhost:8000/api/createshorurl`

In POST request set field `full_url` and value should be your's custom URL. 
As response you'll get short URL.

4. Set JWT in header request and go to Route 3.
POST `http://localhost:8000/api/getfullurl`

In POST request set field `short_url` and value that you get in previous request. 
As response you'll get full URL.