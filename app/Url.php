<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Url extends Model
{
    
    public $timestamps = false;
    protected $urlLenght = 7; 
    
    public function createShortUrl(){
        $shortUrl =  Str::random($this->urlLenght);
        return $shortUrl;
    }
    
}
