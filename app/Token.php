<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Str;

class Token extends Model
{
    protected $tokenExpireTime = 600; // Expire time in seconds;
    protected $timezone;
    
    // JWT
    protected $header;
    protected $payload;
    protected $secret = 'asdasdasdsadasdasdasdasdasdasdasdasdasdasdadadasdad';
    protected $algorithm = [ "HS256", "HS384", "HS512"];
    
    public function __construct() {
        $this->timezone = config('app.timezone');
    }

    /** 
    * Get header Authorization
    * */
    public function getAuthorizationHeader(){
            $headers = null;
            if (isset($_SERVER['Authorization'])) {
                $headers = trim($_SERVER["Authorization"]);
            }
            else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
                $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = apache_request_headers();
                // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
                $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
                //print_r($requestHeaders);
                if (isset($requestHeaders['Authorization'])) {
                    $headers = trim($requestHeaders['Authorization']);
                }
            }
            return $headers;
    }
    
    /**
     * get access token from header
     * */
    public function getBearerToken() {
        $headers = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
    
    

    protected function getTokenExpirationTimestamp(){
        
        $date = Carbon::now();
        $date->setTimezone('CET');

        return $timestamp = $date->getTimestamp() + $this->tokenExpireTime;
        
    }

    public function createJWT(User $user){

        $randIndex = array_rand($this->algorithm);

        $algorithm = $this->algorithm[$randIndex];
        
        // Create the token header
        $header = json_encode([
            'typ' => 'JWT',
            'alg' =>  $algorithm
        ]);
        
        // Create the token payload
        $payload = json_encode([
            "name" => $user->name,
            "email" => $user->email,
            "exp" => $this->getTokenExpirationTimestamp(),
        ]);
        
        // Encode Header
        $base64UrlHeader = $this->base64UrlEncode($header);

        // Encode Payload
        $base64UrlPayload = $this->base64UrlEncode($payload);
        
        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $this->secret, true);
        
        
        // Encode Signature to Base64Url String
        $base64UrlSignature = $this->base64UrlEncode($signature);

        // Create JWT
        return $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
        
    }


    /**
     * Encode data to Base64URL
     * @param string $data
     * @return boolean|string
     */
    protected function base64UrlEncode($data)
    {
      // First of all you should encode $data to Base64 string
      $b64 = base64_encode($data);

      // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
      if ($b64 === false) {
        return false;
      }

      // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
      $url = strtr($b64, '+/', '-_');

      // Remove padding character from the end of line and return the Base64URL result
      return rtrim($url, '=');
    }

    /**
     * Decode data from Base64URL
     * @param string $data
     * @param boolean $strict
     * @return boolean|string
     */
    protected function base64UrlDecode($data, $strict = false)
    {
      // Convert Base64URL to Base64 by replacing “-” with “+” and “_” with “/”
      $b64 = strtr($data, '-_', '+/');

      // Decode Base64 string and return the original data
      return base64_decode($b64, $strict);
    }
    
    
    protected function decodeJWT($jwt): array{

        // split the token
        $tokenParts = explode('.', $jwt);
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $signatureProvided = $tokenParts[2];

        // check the expiration time - note this will cause an error if there is no 'exp' claim in the token
        $expiration = Carbon::createFromTimestamp(json_decode($payload)->exp); 
        $tokenExpired = (Carbon::now()->timezone($this->timezone)->diffInSeconds($expiration, false) < 0);

        // build a signature based on the header and payload using the secret
        $base64UrlHeader = $this->base64UrlEncode($header);
        $base64UrlPayload = $this->base64UrlEncode($payload);
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $this->secret, true);
        $base64UrlSignature = $this->base64UrlEncode($signature);

        // verify it matches the signature provided in the token
        $signatureValid = ($base64UrlSignature === $signatureProvided);

        $tokenData = [$tokenExpired, $signatureValid]; // Array with two bool
        
        return $tokenData;

        
    }
    
    public function isApiTokenExpired($jwt): bool{
        
        if(empty($jwt)){
            // Token is not set
            return true;
        }
        
        $isExpired = (bool) $this->decodeJWT($jwt)[0];
        
        return $isExpired;
        
    }
    
    public function isSignatureValid($jwt): bool{
        
        if(empty($jwt)){
            // Token is not set
            return false;
        }
        
        $isValid = (bool) $this->decodeJWT($jwt)[1];
        
        return $isValid;
        
    }
    
    
}
