<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\{Token, User, Url};



class ApiLoginController extends Controller
{
    
    protected $jwt = NULL;
    public $token;

    public function __construct(){

        $this->token = new Token();
        $this->jwt = $this->token->getBearerToken();

    }


    public function login(Request $request): string {
        
        $login = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
       

        if( !Auth::attempt($login) ){
            
            // Unsuccess login
            return response(['message' => 'Invalid login credentials.']);
            
        }else{
        
            // Success login

            $currentToken = Auth::user()->api_token;
            
            if(!$this->token->isApiTokenExpired($currentToken)){
                
                // Token is still valid
                return response([
                    'token' => $currentToken, 
                ]);
                
            }else{
                
                // Token expired create new token.
                $currentUser = \App\User::find( Auth::user()->id );

                $jwt = $this->token->createJWT( Auth::user() );

                $currentUser->api_token = $jwt;
                $currentUser->save();

                return response(['token' => $jwt, ]);
                
            }

        }

    }
    
    public function createShortURL(Request $request) : string{

        $jwt = $this->token->getBearerToken();
        
        if(!$this->token->isApiTokenExpired($jwt)){
            
            $fullUrl = $request->input('full_url');

            $url = new Url();
            $shortUrl = $url->createShortUrl();
           
            $url->full_url = $fullUrl;
            $url->short_url = $shortUrl;
            $url->save();
            
            return response(['short_url' => $shortUrl]);
            
        }else{
            return response(['message' => 'Token is not valid.']);
        }
        
    }
    
    public function getFullURL(Request $request) : string{

        $jwt = $this->token->getBearerToken();

        if(!$this->token->isApiTokenExpired($jwt)){

            $shortUrl = $request->input('short_url');
            
            $url = new Url();
            $urlData = $url->where([
                ['short_url', '=', $shortUrl],
            ])->first();
            
            $fullUrl = $urlData->full_url;
            
            return response(['short_url' => $fullUrl]);
 
        }else{
            return response(['message' => 'Token is not valid.']);
        }

    }

    
}
