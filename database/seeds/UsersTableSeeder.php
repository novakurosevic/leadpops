<?php

use Illuminate\Database\Seeder;
//use App\Token;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
//        $token = new Token();
//        $newToken = $token->createNewToken();
//        $tokenExpireTime = $token->getTokenExpirationTime();
        
        DB::table('users')->insert([
            'name' => 'Test User',
            'email' => 'test@test.com',
            'password' => bcrypt('123'),
            'api_token' => '',
        ]);
        
    }
}
